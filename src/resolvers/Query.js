const Query =
{
    user(parent, args, { db }, info) {
        return {
            id: '152585',
            name: "Mohammad Sadeghian",
            sex: 'Male',
            email: 'mosa5445@gmail.com',
            grade: [5, 19.25, 18, 9.75, 20]
        }
    },
    users(parents, args, { db }, info) {
        return db.users
    },
    posts(parents, args, { db }, info) {
        if (!args.q)
            return db.posts
        return db.posts.filter((post) => {
            const Title_Search = post.title.toLowerCase().match(args.q.toLowerCase())
            const Content_Search = post.content.toLowerCase().match(args.q.toLowerCase())
            return Title_Search || Content_Search
        })
    }
}

export { Query as default }
